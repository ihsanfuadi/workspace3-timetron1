#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "DHT.h"

#define DHTTYPE DHT11

// Replace with your network credentials
const char* ssid     = "NYAYUR";
const char* password = "dtech-en9ineerin999";

// Set web server port number to 80
WiFiServer server(80);
uint8_t DHTpin=D4;
DHT dht (DHTpin,DHTTYPE);
float Temperature;
float Humidity;

// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String output5State = "off";
String output4State = "off";

// Assign output variables to GPIO pins
const int output5 = 5;
const int output4 = 4;

void setup() {
  Serial.begin(115200);
  // Initialize the output variables as outputs
  pinMode(output5, OUTPUT);
  pinMode(output4, OUTPUT);
  pinMode(DHTpin,INPUT);

  // Set outputs to LOW
  digitalWrite(output5, HIGH);
  digitalWrite(output4, LOW);
  dht.begin();
  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.on("/",handle_OnConnect);
  server.onNotFound(handle_NotFound);
  server.begin();
  Serial.println("HTTP SERVER started");
}

void loop() {
  server.handleClient();
  
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            // turns the GPIOs on and off
            if (header.indexOf("/?Pilih+delay=3") != 0) {
              Serial.println("client pilih 3");
              //output5State = "off";
              digitalWrite(output5, LOW);
              delay(3000);
              digitalWrite(output5, HIGH);

            } 
            else if (header.indexOf("/?pilih+delay=4") != 0) {
              Serial.println("client pilih 4");
              //output5State = "off";
              digitalWrite(output5, LOW);
              delay(4000);
              digitalWrite(output5,HIGH);
            } 
            else if (header.indexOf("/?pilih+delay=5") !=0) {
              Serial.println("client pilih 5");
              //output4State = "on";
              digitalWrite(output5, LOW);
              delay(5000);
              digitalWrite(output5, HIGH);
            } 
            else if (header.indexOf("/?pilih+delay=6") != 0) {
              Serial.println("client pilih 6");
              //output4State = "off";
              digitalWrite(output5, LOW);
              delay(6000);
              digitalWrite(output5, HIGH);

            }
            
            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons
            // Feel free to change the background-color and font-size attributes to fit your preferences
            //client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            //client.println(".button { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
            //client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            //client.println(".button2 {background-color: #77878A;}</style></head>");
            // Web Page Heading
            client.println("<body><h1>ESP8266 Web Server</h1>");
            client.println("<form>");
            client.println("<table>");
            client.println("<tr>");
            client.println("<td>Pilih Delay</td>");
            client.println("<td><select name=\"Pilih delay\"><br>");
            client.println("<option value=\"-1\"selected>select..</option>");
            client.println("<option value=\"3\">3</option>");
            client.println("<option value=\"4\">4</option>");
            client.println("<option value=\"5\">5</option>");
            client.println("<option value=\"6\">6</option>");
            client.println("<option value=\"7\">7</option>");
            client.println("</select></td>");
            client.println("<td colspan=\"1\"><input type=\"submit\" value=\"send\">");
            client.println("<tr>");
            client.println("<td>");
            client.println("<input type=\"reset\"value=\"Reset\">");
            client.println("</td>");
            client.println("")
            // Display current state, and ON/OFF buttons for GPIO 5
            //client.println("<p>GPIO 5 - State " + output5State + "</p>");
            // If the output5State is off, it displays the ON button
            //client.println("<p><a href=\"/5/on\"><button class=\"button\">ON</button></a></p>");
            // Display current state, and ON/OFF buttons for GPIO 4
            //client.println("<p>GPIO 4 - State " + output4State + "</p>");
            // If the output4State is off, it displays the ON button
            //client.println("<p><a href=\"/4/on\"><button class=\"button\">ON</button></a></p>");
            client.println("</table></form></body></html>");
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}
void handle_OnConnect(){
  Temperature=dht.readTemperature();
  Humidity=dht.readHumidity();
  server.send(200,"text/html",SendHTML(Temperature,Humidity));
  }
void handle_NotFound(){
  Server.send(404,"text/plain","Not Found");
  }
  String SendHTML(float Temperaturestat,float Humiditystat){
  String ptr ="<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>ESP8266 Weather Report</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;}\n";
  ptr +="p {font-size: 24px;color: #444444;margin-bottom: 10px;}\n";
  ptr +="</style>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<div id=\"webpage\">\n";
  ptr +="<h1>ESP8266 NodeMCU Weather Report</h1>\n";
  
  ptr +="<p>Temperature: ";
  ptr +=(int)Temperaturestat;
  ptr +="°C</p>";
  ptr +="<p>Humidity: ";
  ptr +=(int)Humiditystat;
  ptr +="%</p>";
  
  ptr +="</div>\n";
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}
  
