//http://arduino-er.blogspot.com/2016/05/nodemcuesp8266-act-as-ap-access-point_3.html

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <DHT.h>

#define DHTTYPE DHT11

const char *ssid = "ihsan";
const char *password = "fuadi123";
int stateLED = HIGH;

ESP8266WebServer server(80);
uint8_t DHTpin = D2;
DHT dht(DHTpin, DHTTYPE);
float Temperature;
float Humidity;

void handleRoot() {
  response();
}

void handleLedOn() {
  stateLED = LOW;
  digitalWrite(D2, stateLED);
  response();
}

void handleLedOff() {
  stateLED = HIGH;
  digitalWrite(D2, stateLED);
  response();
}

const String HtmlHtml = "<html><head>"
                        "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" /></head>";
const String HtmlHtmlClose = "</html>";
const String HtmlTemp = "<p>Temperature:</p>";
const String HtmlHumid = "<p>Himidity:</p>";
const String HtmlTitle = "<h1>Arduino-er: ESP8266 AP WebServer and Temperature</h1><br/>\n";
const String HtmlLedStateLow = "<big>LED is now <b>ON</b></big><br/>\n";
const String HtmlLedStateHigh = "<big>LED is now <b>OFF</b></big><br/>\n";
const String HtmlButtons =
  "<a href=\"LEDOn\"><button style=\"display: block; width: 100%;\">ON</button></a><br/>"
  "<a href=\"LEDOff\"><button style=\"display: block; width: 100%;\">OFF</button></a><br/>";

void response() {
  String htmlRes = HtmlHtml + HtmlTitle;
  if (stateLED == LOW) {
    htmlRes += HtmlLedStateLow;
  } else {
    htmlRes += HtmlLedStateHigh;
  }

  htmlRes += HtmlButtons;
  htmlRes += HtmlHtmlClose;
  htmlRes += HtmlTemp;
  htmlRes += HtmlHumid;
  server.send(200, "text/html", htmlRes);

}

void setup() {
  delay(1000);
  Serial.begin(9600);
  pinMode(DHTpin, INPUT);
  dht.begin();
  Serial.println("connecting to");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.println("Got IP:"); Serial.println(WiFi.localIP());

  server.on("/", handle_OnConnect);
  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server strated");
  Serial.println();
  dht.begin();
  WiFi.softAP(ssid, password);

  IPAddress apip = WiFi.softAPIP();
  Serial.print("visit: \n");
  Serial.println(apip);
  server.on("/", handleRoot);
  server.on("/LEDOn", handleLedOn);
  server.on("/LEDOff", handleLedOff);
  server.begin();
  Serial.println("HTTP server beginned");
  pinMode(D2, OUTPUT);
  digitalWrite(D2, stateLED);
}

void loop() {
  float kelembapan = dht.readHumidity();
  float suhu = dht.readTemperature();
  Serial.println("Suhu:");
  Serial.println(suhu);
  Serial.println("kelembapan:");
  Serial.println(kelembapan);
  delay(1000);
  server.handleClient();
}

void handle_OnConnect() {
  Temperature = dht.readTemperature();
  Humidity = dht.readHumidity();
  server.send(200, "text/html",SendHTML(Temperature, Humidity));
}
void handle_NotFound() {
 server.send(404, "text/plain", "Not found");
}
