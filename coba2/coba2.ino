// http://www.osoyoo.com/driver/nodemcu-webserver.ino
// http://osoyoo.com/2018/09/07/lesson-18-use-nodemcu-as-iot-web-server/
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include "DHT.h"

#define DHTTYPE DHT11
uint8_t DHTPIN = D2;
DHT dht (DHTPIN, DHTTYPE);
// Replace with your network credentials
const char* ssid = "ihsan"; //replace ssid value with your own wifi hotspot name
const char* password = "fuadi123"; //replace password value with your wifi password
 
ESP8266WebServer server(80);   //instantiate server at port 80 (http port)
 
String page = "";
int LEDPin = D3;
void setup(void){
  //the HTML of the web page

  //make the LED pin output and initially turned off
  pinMode(LEDPin, OUTPUT);
  digitalWrite(LEDPin, LOW);
   
  delay(1000);
  Serial.begin(115200);
  WiFi.begin(ssid, password); //begin WiFi connection
  Serial.println("");
 
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

 
   
  server.on("/", [](){
    server.send(200, "text/html", page);
  });
  server.on("/TurnOnLED", [](){
    page = "<h1>NodeMCU Web Server for IOT</h1><p><strong>LED is ON</strong></p>";
  page +="<p><a href=\"TurnOnLED\"><button>Turn on LED</button></a>&nbsp;<a href=\"TurnOffLED\"><button>Turn off LED</button></a></p>";
  page +="<p><a href=\"GetTempHumid\"><button>Display Temperature and Humidity</button>";
    server.send(200, "text/html", page);
    digitalWrite(LEDPin, HIGH);
    delay(1000);
  });
  server.on("/TurnOffLED", [](){
      page = "<h1>NodeMCU Web Server for IOT</h1><p><strong>LED is OFF</strong></p>";
  page +="<p><a href=\"TurnOnLED\"><button>Turn on LED</button></a>&nbsp;<a href=\"TurnOffLED\"><button>Turn off LED</button></a></p>";
  page +="<p><a href=\"GetTempHumid\"><button>Display Temperature and Humidity</button>";
    server.send(200, "text/html", page);
    digitalWrite(LEDPin, LOW);
    delay(1000); 
  });
   server.on("/GetTempHumid", [](){
      int chk=DHT.read(DHTPIN);
     String msg="<p>Real time temperature: ";
     msg += msg DHT.temperature;
     msg += msg" C ;real time Humidity: " ;
     msg+=msgDHT.humidity ;
     msg+=msg"%</p>";
 
   
  page = "<h1>NodeMCU Web Server for IOT</h1>"+msg;
  
  page +="<p><a href=\"TurnOnLED\"><button>Turn on LED</button></a>&nbsp;<a href=\"TurnOffLED\"><button>Turn off LED</button></a></p>";
  
  page +="<p><a href=\"GetTempHumid\"><button>Display Temperature and Humidity</button>";
    server.send(200, "text/html", page);
    delay(1000); 
  });
  server.begin();
  Serial.println("Web server started!");
}
 
void loop(void){
  server.handleClient();
}
